<?php
namespace App;

class DueDateCalculator
{
    const START_WORKING_HOUR = '09:00';
    
    const END_WORKING_HOUR = '17:00';
    
    public function calculateDueDate($submitDate, $turnaroundTime)
    {
        if (!$this->checkDateBetweenWorkingHours($submitDate)) {
            throw new \Exception('The given date is not in the working hours!');
        }
        
        if (!$this->checkTimeIsValid($turnaroundTime)) {
            throw new \Exception('The given time is not valid!');
        }
        
        $dueDate = '';
        $days = ceil($turnaroundTime/$this->getDailyWorkingHours());
        foreach (range(1, $days) as $index) {
            if ($dueDate == '') {
                $dueDate = new \DateTime($submitDate);
            }
            
            if ($index == $days && $turnaroundTime % $this->getDailyWorkingHours() > 0) {
                $hour = $turnaroundTime % $this->getDailyWorkingHours();
                $dueDate->modify("+$hour hours");
            } else {
                $dueDate->modify('+1 day');
                if ($this->dateIsWeekend($dueDate)) {
                    $dueDate->modify('+2 day');
                }
            }
        }
        return $dueDate->format('Y-m-d H:i');
    }
    
    private function checkDateBetweenWorkingHours($date)
    {
        $submitDate = new \DateTime($date);
        $startDate =  new \DateTime($submitDate->format('Y-m-d').self::START_WORKING_HOUR);
        $endDate = new \DateTime($submitDate->format('Y-m-d').self::END_WORKING_HOUR);
        if ($submitDate >= $startDate && $submitDate <= $endDate && !$this->dateIsWeekend($submitDate)) {
            return true;
        }
        return false;
    }
    
    private function checkTimeIsValid($time)
    {
        return is_int($time) && $time > 0;
    }
    
    private function dateIsWeekend(\DateTime $date)
    {
        return $date->format('N') >= 6;
    }
    
    private function getDailyWorkingHours()
    {
        $start = new \DateTime(self::START_WORKING_HOUR);
        $end = new \DateTime(self::END_WORKING_HOUR);
        $diff = $end->diff($start);
        return $diff->h;
    }
}
