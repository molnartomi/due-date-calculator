<?php
namespace Tests;

use App\DueDateCalculator;

class TestDueDateCalculator extends \PHPUnit_Framework_TestCase
{
    public function testFriday()
    {
        $dueDateCalculator = new DueDateCalculator();
        $dueDate = $dueDateCalculator->calculateDueDate('2017-07-07 09:12', 17);
        $this->assertEquals('2017-07-11 10:12', $dueDate);
    }
    
    public function testInvalidSubmitDate()
    {
        $hasException = false;
        try {
            $dueDateCalculator = new DueDateCalculator();
            $dueDateCalculator->calculateDueDate('2017-07-06 08:30', 8);
        } catch (\Exception $e) {
            $hasException = true;
        }
        $this->assertTrue($hasException);
    }
    
    public function testHalfDay()
    {
        $dueDateCalculator = new DueDateCalculator();
        $dueDate = $dueDateCalculator->calculateDueDate('2017-07-06 09:12', 4);
        $this->assertEquals('2017-07-06 13:12', $dueDate);
    }
}
